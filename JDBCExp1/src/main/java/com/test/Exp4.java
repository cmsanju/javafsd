package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

public class Exp4 {
	
	public static void main(String[] args) throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/b2java", "root", "password");
		
		//Statement stmt = con.createStatement();
		
		//String sql = "create table b2fsd1(emp_id int, emp_name varchar(50), emp_salary double)";
		
		//stmt.execute(sql);
		
		PreparedStatement pst = con.prepareStatement("insert into b2fsd1 values(?,?,?)");
		
		Employee emp = new Employee();
		
		emp.setEmp_id(1);
		emp.setEmp_name("Hello");
		emp.setEmp_salary(123123.23);
		
		pst.setInt(1, emp.getEmp_id());
		pst.setString(2, emp.getEmp_name());
		pst.setDouble(3, emp.getEmp_salary());
		
		pst.execute();
		
		System.out.println("Stored Data");
		
		con.close();
	}

}
