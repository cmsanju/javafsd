package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//step 1 load the driver class
		Class.forName("com.mysql.jdbc.Driver"); // oracle.jdbc.driver.OracleDriver
		
		//step 2 create connection object
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/b2java", "root", "password");
		
		con.setAutoCommit(false);
		
		//step 3 create statement object
		
		Statement stmt = con.createStatement();
		
		//execute query
		
		//stmt.execute("create table b2fsd(emp_id int, emp_name varchar(50), emp_city varchar(50))");
		
		String sql1 = "insert into b2fsd values(5, 'Apple', 'Blr')";
		
		String sql2 = "insert into b2fsd values(6,'Dell', 'BTM')";
		
		String sql3 = "update b2fsd set emp_city='Koramangla' where emp_id = 6";
		
		//String sql4 = "delete from b2fsd where emp_id =6";
		
		stmt.addBatch(sql1);
		stmt.addBatch(sql2);
		stmt.addBatch(sql3);
		//stmt.addBatch(sql4);
		
		stmt.executeBatch();
		
		//con.commit();
		con.rollback();
		
		String sql = "select * from b2fsd";
		
		ResultSet rs = stmt.executeQuery(sql);
		
		ResultSetMetaData rsmd = rs.getMetaData();
		
		System.out.println(rsmd.getColumnCount());
		System.out.println(rsmd.getColumnName(2));
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		
		con.close();
	}

}
