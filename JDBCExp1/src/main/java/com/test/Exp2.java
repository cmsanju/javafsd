package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Exp2 {
	
	public static void main(String[] args) throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/b2java", "root", "password");
		/*
		PreparedStatement pst = con.prepareStatement("insert into b2fsd values(?,?,?)");
		
		pst.setInt(1, 4);
		pst.setString(2, "yadav");
		pst.setString(3, "Blr");
		
		
		PreparedStatement pst = con.prepareStatement("update b2fsd set emp_name=? where emp_id=?");
		
		pst.setString(1, "Manisha");
		pst.setInt(2, 4);
		*/
		
		PreparedStatement pst = con.prepareStatement("select * from b2fsd");
		
		ResultSet rs = pst.executeQuery();
		
		while(rs.next())
		{
		  System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		
		con.close();
	}

}
