<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
		<!-- 1 declaration tag 2 expression tag 3 scriplet tag -->
		
		<%!
			
			int x = 20;
			int y = 30;
			
			public int add()
			{
				return x+y;
			}
		
		%>
		
		<%= add() %>
		
		Current Data and Time : <%= new Date() %>
		
		<%
			Date date = new Date();
			
			out.println("Date and Time : "+date);
		%>
</body>
</html>