package com.test;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class TestCalculator {
	
	
	@Mock
	Calculator obj;
	
	@BeforeClass
	public static void beforeClass()
	{
		System.out.println("Before Class");
	}
	
	@AfterClass
	public static void afterClass()
	{
		System.out.println("After Class");
	}
	
	@Before
	public void setUp()
	{
		System.out.println("before test method");
		
		obj = new Calculator();
		
		//MockitoAnnotations.initMocks(Calculator.class);
		
		//obj = Mockito.mock(Calculator.class);
	}
	
	@After
	public void setDown()
	{
		System.out.println("after test method");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("Test add method");
		
	//	when(obj.add(30, 30)).thenReturn(60);
		
		assertEquals(100, obj.add(90, 10));
	}
	
	@Test
	public void testSub()
	{
		System.out.println("Test sub method");
		
		assertEquals(50, obj.sub(200, 150));
	}
	@Test
	public void testTestMsg()
	{
		System.out.println("Test string msg");
		
		assertEquals("java", obj.textMsg("java"));
	}

}
