package com.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class MockTest {
	
	
	@Test
	public void testAdd()
	{
		Calculator obj = Mockito.mock(Calculator.class);
		
		when(obj.add(40, 30)).thenReturn(70);
		
		assertEquals(obj.add(40, 30), 70);
	}
	
	@Test(expected = RuntimeException.class)
	public void testExpectedException()
	{
		List<String> list = Mockito.mock(List.class);
		
		when(list.get(Mockito.anyInt())).thenThrow(new RuntimeException("RuntiemException"));
		
		list.get(0);
	}

}
