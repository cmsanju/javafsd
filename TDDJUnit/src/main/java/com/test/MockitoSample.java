package com.test;

import java.util.ArrayList;
import java.util.List;

public class MockitoSample {
	
	public MockService mockService;
	
	
	public MockitoSample(MockService mockService)
	{
		this.mockService = mockService;
	}
	
	public List<String> getListData(String names)
	{
		List<String> getList = new ArrayList<String>();
		
		List<String> getNames = mockService.getNameList(names);
		
		for(String name : getNames)
		{
			if(name.contains("java"))
			{
				getList.add(name);
			}
		}
		
		return getList;
	}

}
