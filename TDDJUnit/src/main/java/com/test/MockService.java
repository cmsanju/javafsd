package com.test;

import java.util.List;

public interface MockService {
	
	public List<String> getNameList(String names);

}
