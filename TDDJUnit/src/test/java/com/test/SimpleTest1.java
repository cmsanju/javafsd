package com.test;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class SimpleTest1 {
	
	@Mock
	CalculatorService calService;
	
	@Before
	public void setUp()
	{
		//calService = new CalculatorService();// JUnit
		
		calService = Mockito.mock(CalculatorService.class);// Mockito
	}
	@After
	public void setDown()
	{
		calService = null; // after test method asssertEquals method data will go to null 
	}
	
	
	
	@Test
	public void testAdd()
	{
		//calService = Mockito.mock(CalculatorService.class);
		
		//assertEquals(300, calService.add(200, 100));
		when(calService.add(200, 100)).thenReturn(0);
	}
	@Test
	public void testReverse()
	{
		when(calService.nameTest("java")).equals("avaj");
	}

}
