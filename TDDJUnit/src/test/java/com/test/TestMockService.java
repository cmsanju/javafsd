package com.test;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class TestMockService {
	
	/*
	
	@Test
	public void testNameList()
	{
		MockService obj = new MockStub();
		
		MockitoSample obj1 = new MockitoSample(obj);
		
		List<String> getNames = obj1.getListData("java");
		
		assertEquals(1, getNames.size());
	}
	*/
	
	@Test
	public void testNameList1()
	{
		MockService obj = mock(MockService.class);
		
		List<String> getNames = Arrays.asList("java", ".net", "Spring", "jee");
		
		when(obj.getNameList("java")).thenReturn(getNames);
		
		MockitoSample obj1 = new MockitoSample(obj);
		
		List<String> names = obj1.getListData("java");
		
		System.out.println(names);
		
		assertEquals(1, names.size());
	}

}
