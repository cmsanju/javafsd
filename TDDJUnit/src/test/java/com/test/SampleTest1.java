package com.test;


import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class SampleTest1 {
	
	@Mock
	Simple simple;
	
	
	@Before
	public void setUp()
	{
		//simple  = new Simple();
		
		simple = Mockito.mock(Simple.class);
		
	}
	
	@Test
	public void testAverageSalary()
	{
		//assertEquals(1100, simple.avgeSalary(1000, 100));//JUnit
		
		when(simple.avgeSalary(1000, 100)).thenReturn(1100);//Mockito
	}

}
