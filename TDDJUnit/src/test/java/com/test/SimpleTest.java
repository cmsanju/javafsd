package com.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SimpleTest {
	
	Simple simple;
	
	static List<String> onames;
	
	@BeforeAll
	public static void beforeClass()
	{
		onames = Arrays.asList("lenovo", "sony", "dell");
		
		System.out.println("Before class");
	}
	
	@AfterAll
	public static void afterClass()
	{
		System.out.println("after class");
	}
	
	@BeforeEach
	public void setUp()
	{
		simple = new Simple();
		
		System.out.println("before test method");
	}
	@AfterEach
	public void setDown()
	{
		simple = null;
		
		System.out.println("after test method");
	}
	
	@Test
	public void testAverageSalary()
	{
		assertEquals(1100, simple.avgeSalary(1000, 100));
		
		System.out.println("Test method 1");
	}
	
	@Test
	public void testTotalBal()
	{
		assertEquals(1150, simple.totalBal(1100, 50));
		
		System.out.println("Test method 2");
	}
	@Test
	public void testOnames()
	{
		assertEquals(3, simple.ordersList(onames));
	}

}
