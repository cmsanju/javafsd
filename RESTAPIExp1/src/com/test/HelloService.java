package com.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class HelloService 
{
	
	@GET
	@Path("/xml")
	@Produces(MediaType.TEXT_XML)
	public String xmlResponse()
	{
		return "<?xml version=\"1.0\"?>" +"<hello> This is XML Response"+"</hello>";
	}
}
