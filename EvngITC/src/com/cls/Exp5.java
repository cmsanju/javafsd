package com.cls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Employee implements Comparable<Employee>
{
	private int id;
	private String name;
	private String city;
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, String city)
	{
		this.id = id;
		this.name = name;
		this.city = city;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public int compareTo(Employee emp)
	{
		return this.id - emp.id;
	}
	
}

class NameComparator implements Comparator<Employee>
{
	public int compare(Employee e1 , Employee e2)
	{
		return e2.getName().compareTo(e1.getName());
	}
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		List<Employee> emp = new ArrayList<Employee>();
		
		emp.add(new Employee(11, "nikhil", "blr"));
		emp.add(new Employee(9, "guptha", "Kolkata"));
		emp.add(new Employee(7, "saroja", "hyd"));
		emp.add(new Employee(4, "saunak", "blr"));
		
		//System.out.println(emp);
		
		Collections.sort(emp);
		
		System.out.println("ID sorting");
		
		for(Employee data : emp)
		{
			System.out.println(data.getId()+" "+data.getName()+" "+data.getCity());
		}
		
		Collections.sort(emp, new NameComparator());
		
		System.out.println("Name sorting ");
		
		for(Employee data1 : emp)
		{
			System.out.println(data1.getId()+" "+data1.getName()+" "+data1.getCity());
		}
		
	}

}
