package com.cls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//Collection data = new ArrayList();
		//List data1 = new ArrayList();
		
		//ArrayList data = new ArrayList();
		
		LinkedList data = new LinkedList();
		
		data.add(10);
		data.add("java");
		data.add(10);
		data.add("java");
		data.add(34.49);
		data.add(63.37f);
		data.add(false);
		data.add('A');
		
		data.set(4, "ITC");
		
		System.out.println(data);
		
		System.out.println(data.size());
		
		System.out.println(data.contains(10));
		
		System.out.println(data.get(3));
		
		//Iterator  ListIterator
		
		//Iterator itr = data.iterator();
		
		ListIterator itr = data.listIterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
			
			if(data.get(4).equals(itr.next()))
			{
				System.out.println();
			}
		}
		
		System.out.println("=============");
		
		while(itr.hasPrevious())
		{
			System.out.println(itr.previous());
		}
	}

}
