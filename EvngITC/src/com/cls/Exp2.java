package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class Exp2 {
	
	public static void main(String[] args) {
		
		HashSet<String> data = new HashSet<String>();
		
		data.add("java");
		data.add("php");
		data.add("spring");
		data.add("hello");
		data.add("test");
		data.add("apple");
		data.add("mac");
		data.add("java");
		data.add("spring");
		
		
		System.out.println(data);
		
		Iterator<String> itt = data.iterator();
		
		
		
		LinkedHashSet<String> data1 = new LinkedHashSet<String>();
		
		data1.add("java");
		data1.add("php");
		data1.add("spring");
		data1.add("hello");
		data1.add("test");
		data1.add("apple");
		data1.add("mac");
		data1.add("java");
		data1.add("spring");
		
		System.out.println(data1);
		
		System.out.println(data1.size());
		
		Iterator<String> itr = data1.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		for(String nm : data1)
		{
			System.out.println(nm);
		}
		
		data1.clear();
		
		System.out.println(data1);
		
		TreeSet<String> data2 = new TreeSet<String>();
		
		data2.add("java");
		data2.add("php");
		data2.add("spring");
		data2.add("hello");
		data2.add("test");
		data2.add("apple");
		data2.add("mac");
		data2.add("java");
		data2.add("spring");
		
		System.out.println(data2);
	}

}
