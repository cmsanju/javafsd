package com.cls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Exp3 {
	
	public static void main(String[] args) {
		
		HashMap<String, Integer> data = new HashMap<String, Integer>();
		
		data.put("asus", 200);
		data.put("dell", 30);
		data.put("sony", 5000);
		data.put("lenovo", 49);
		data.put("ma", 9900);
		data.put("apple", 34344);
		data.put("dell", 575);
		
		System.out.println(data);
		
		//Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		LinkedHashMap<String, Integer> data1 = new LinkedHashMap<String, Integer>();
		
		data1.put("asus", 200);
		data1.put("dell", 30);
		data1.put("sony", 5000);
		data1.put("lenovo", 49);
		data1.put("ma", 9900);
		data1.put("apple", 34344);
		data1.put("dell", 575);
		
		System.out.println(data1);
		
		Iterator<Entry<String, Integer>> itr = data1.entrySet().iterator();
		
		while(itr.hasNext()) {
			
			Entry<String, Integer> etr = itr.next();
			
			System.out.println("Product : "+etr.getKey()+" Price : "+etr.getValue());
		}
		
		for(String key : data1.keySet())
		{
			System.out.println("Key : "+key+" Value : "+data1.get(key));
		}
		
		TreeMap<String, Integer> data2 = new TreeMap<String, Integer>();
		
		data2.put("asus", 200);
		data2.put("dell", 30);
		data2.put("sony", 5000);
		data2.put("lenovo", 49);
		data2.put("ma", 9900);
		data2.put("apple", 34344);
		data2.put("dell", 575);
		data2.put(null, 848);
		
		System.out.println(data);
		
		for(String key : data2.keySet())
		{
			System.out.println("Key : "+key+" Value : "+data2.get(key));
		}
	}

}
