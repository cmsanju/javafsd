package com.cls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Exp4 {
	
	public static void main(String[] args) {
		
		List<String> empList = new ArrayList<String>();
		
		empList.add("Java");
		empList.add("eMP2");
		empList.add("Hello");
		empList.add("mac");
		empList.add("itc");
		
		
		System.out.println(empList);
		
		
		for(String names : empList)
		{
			System.out.println(names);
		}
		
		Iterator<String> itr = empList.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
