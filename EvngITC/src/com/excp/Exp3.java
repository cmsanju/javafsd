package com.excp;

public class Exp3 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(38/0);
		}
		catch(Exception e)
		{
			//using getMessage();
			
			System.out.println(e.getMessage());
			
			//printing Exception class obj
			
			System.out.println(e);
			
			//using printStackTrace();
			
			e.printStackTrace();
		}
	}

}
