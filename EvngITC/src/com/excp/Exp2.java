package com.excp;

public class Exp2 {
	
	public static void main(String[] args) {
		try
		{
			Class.forName("com.excp.Exp2");
			
			System.out.println("hello");
			System.out.println(747/3);
			System.out.println("tEST");
			
			int[] ar = {23,34,53};
			
			System.out.println(ar[2]);
			
			String str = "Hello";
			
			System.out.println(str.charAt(7));
			
			String name = "admin";
			
			System.out.println(name.equals("admin"));
			
			int x = Integer.parseInt("48");
		}
	//	System.out.println();
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch(ClassNotFoundException cfn)
		{
			System.out.println("enter class name");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check your array size");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check your string length");
		}
		catch(NullPointerException npe)
		{
			System.out.println("please enter name");
		}
		catch(NumberFormatException nfe)
		{
			System.out.println("please enter numerics only");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
		//System.out.println();
		finally {
			System.out.println("i am from finally.");
		}
		
		System.out.println("Hello");
	}

}

/**
 * 
 * 1 try block
 * 2 catch block
 * 3 finally block
 * 4 throws
 * 5 throw
 * 
 */
