package com.inhs;

import java.util.regex.Pattern;

public class Exp10 {
	
	public static void main(String[] args) {
		
		System.out.println(Pattern.matches("..e", "hie"));
		
		System.out.println(Pattern.matches("[a-zA-Z0-9]{8}", "java123"));
	}

}
