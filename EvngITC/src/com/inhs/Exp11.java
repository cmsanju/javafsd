package com.inhs;

public class Exp11 {
	
	public static void main(String[] args) {
		
		String str1 = "java";
		String str2 = "java";
		
		System.out.println(str2.charAt(3));
		
		String str3 = "hello";
		String str4 = "Java";
		
		String str5 = new String("java");
		String str6 = new String("java");
		
		
		System.out.println(str1 == str2);
		
		System.out.println(str1 == str5);
		
		System.out.println(str1.equals(str5));
		
		System.out.println(str1.hashCode()+" "+str2.hashCode());
		System.out.println(str5.hashCode());
		
		System.out.println(str6.hashCode());
		
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}

}
