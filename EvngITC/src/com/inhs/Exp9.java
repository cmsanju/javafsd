package com.inhs;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exp9 {
	
	public static void main(String[] args) {
		
		String inpstr = "java";
		
		String estr = "Java";
		
		//System.out.println(Pattern.matches(estr, inpstr));
		
		//System.out.println(estr.equalsIgnoreCase(inpstr));
		
		Pattern ptr = Pattern.compile("JaVa");
		
		Matcher mtr = ptr.matcher("JaVa");
		
		while(mtr.find())
		{
			System.out.println("Pattern matches : "+mtr.start()+" "+(mtr.end()-1));
		}
	}

}
