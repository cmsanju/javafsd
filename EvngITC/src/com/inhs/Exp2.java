package com.inhs;

class D
{
	public static void human()
	{
		System.out.println("parent");
	}
	
	public final void cat()
	{
		System.out.println("cat()");
	}
	
	public void book()
	{
		System.out.println("book parent");
	}
}

class E extends D
{
	public void disp()
	{
		System.out.println("disp()");
	}
	
	@Override
	public void book()
	{
		System.out.println("overrided book");
	}
}

class F extends D
{
	public void fox()
	{
		System.out.println("fox()");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		E e = new E();
		
		D.human();
		e.cat();
		e.disp();
	}

}
