package com.inhs;

interface J
{
	int x = 20;
	
	default void show()
	 {
		 
	 }
}

interface K //extends J
{
	int x = 30;
	
	default void show()
	{
		
	}
}

class L implements J, K 
{
	public void disp()
	{
		System.out.println(J.x+" "+K.x);
	}

	//@Override
	public void show() {
		
		J.super.show();
		
		K.super.show();
		
	}
}

public class Exp3 {

}
