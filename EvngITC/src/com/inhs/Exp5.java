package com.inhs;

interface X
{
	void add(int x, int y);
	//void add();
	
	interface Y
	{
		void sub(int x, int y);
	}
}

class Impl1 implements X
{
	//@Override
	public void sub(int x, int y)
	{
		System.out.println(x+y);
	}
	
	@Override
	public void add(int x, int y)
	{
		System.out.println(x-y);
	}
}

public class Exp5 {
	
	public static void main(String[] args) {
		
	}

}
