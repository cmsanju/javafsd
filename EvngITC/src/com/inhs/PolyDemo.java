package com.inhs;

class Parent
{
		public void draw()
		{
			System.out.println("tle");
		}
}

class Child1 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("cle");
	}
}

class Child2 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("rle");
	}
}

public class PolyDemo {
	
	public static void main(String[] args) {
		
		Parent c1 = new Child1();
		
		c1.draw();
		
		Parent c2 = new Child2();
		
		c2.draw();
		
		Parent p1 = new Parent();
	}

}
