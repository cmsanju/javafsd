package com.inhs;

@FunctionalInterface
interface FunTest
{
	String textMessage();
	
}

public class Exp6 {
	
	public static void main(String[] args) {
		
		FunTest obj = new FunTest()
				{
					@Override
					public String textMessage()
					{
						System.out.println("tEST");
						return "Hello";
					}
				};
				
				obj.textMessage();
				
				new FunTest()
				{
					public String textMessage()
					{
						System.out.println("Namelesss object");
						return "Test";
					}
				}.textMessage();
				
				//jdk 1.8 lambda expressions
				
				FunTest obj1 = () -> {
					
					System.out.println("Lambda expression");
					
					return "java";
				};
				
				obj1.textMessage();
		
	}

}
