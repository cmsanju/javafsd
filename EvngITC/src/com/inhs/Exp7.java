package com.inhs;

public class Exp7 {
	
	public static void main(String[] args) {
		
		int[] ar = new int[5];
		
		ar[0] = 10;
		ar[4] = 40;
		
		int[] arr = {12,34,56,78,90};
		
		System.out.println(arr[4]);
		
		System.out.println(arr.length);
		
		for(int i = 0; i < arr.length; i++)
		{
			System.out.println(arr[i]);
		}
		
		for(int x : arr)
		{
			System.out.println(x);
		}
	}

}
