package com.inhs;

interface I1
{
	int x = 94;
	
	void pet();
	
	default void animal()
	{
		System.out.println("Default");
	}
	
	static void dog()
	{
		System.out.println("static");
	}
}

abstract class Abs
{
	public abstract void book();
	
	public void pens()
	{
		System.out.println("abs normal method");
	}
}

class Impl extends Abs implements I1
{
	@Override
	public void book()
	{
		System.out.println("abs overrided");
	}
	
	@Override
	public void pet()
	{
		System.out.println("inf overrided");
	}
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		//I1 i = new I1();
		
		//Abs a = new Abs();
		
		Impl obj = new Impl();
		
		obj.animal();
		obj.pens();
		obj.book();
		obj.pet();
		
		I1.dog();
	}

}
