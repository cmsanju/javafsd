package com.inhs;

class A 
{
	int id = 30;
	String name = "Java";
	
	public void show()
	{
		System.out.println("parent");
	}
}

class B extends A
{
	String cmp = "ITC";
	String city = "Blr";
	
	public void disp()
	{
		System.out.println(id+" "+name+" "+cmp+" "+city);
	}
}


class C extends B
{
	public void animal()
	{
		System.out.println("animal()");
	}
}

class N extends C
{
	public void dog()
	{
		System.out.println("dog()");
	}
}
public class Exp1 {
	
	public static void main(String[] args) {
		
		N b = new N();
		
		b.show();
		b.disp();
		b.animal();
		b.dog();
		
		C c = new C();
		
		c.disp();
		c.show();
		c.animal();
		
		//c.dog();
		
	}

}
