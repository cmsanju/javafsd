package com.test;

class Employee
{
	public Employee()
	{
		System.out.println("Default");
	}
	
	public Employee(int x, String str)
	 {
		 System.out.println("parameterised");
	 }
	
	public Employee(double y)
	{
		System.out.println("overloaded");
	}
	
	public Employee(Employee obj)
	{
		System.out.println("object parameterised");
	}
}

public class EmployeeDemo {
	
	public static void main(String[] args) {
		
		Employee obj1 = new Employee();
		
		Employee obj2 = new Employee(30,"Hello");
		
		Employee obj3 = new Employee(29.84);
		
		Employee obj4 = new Employee(obj1);
		
	}

}
