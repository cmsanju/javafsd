package com.test;

class F
{
	public void add(int x)
	{
		System.out.println("single");
	}
	
	public void add(double y)
	{
		System.out.println("type of the ");
	}
	
	public void add(int x, float y)
	{
		System.out.println("double args");
	}
	
	public void add(float x, int y)
	{
		System.out.println("order of the args");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		F obj = new F();
		
		obj.add(10);
		obj.add(39.45);
		obj.add(20,39.73f);
		obj.add(484.84f, 8);
		
	}

}
