package com.test;

public class Exp4 {
	
	public static void main(String[] args) {
		
		String val1 = "345";//String numerical values
		String val2 = "400";
		
		System.out.println(val1 + val2);//345400
		
		
		//converting string numerical data into fundamental numerical data
		
		
		int x = Integer.parseInt(val1);
		int y = Integer.parseInt(val2);
		
		System.out.println(x+y);//745
		
		boolean b = Boolean.parseBoolean(val1);
		
		System.out.println(b);
		
		double d1 = Double.parseDouble(val1);
		
		double d2 = Double.parseDouble(val2);
		
		System.out.println(d1+d2);
		
		int xx = 3000;
		
		int yy = 4000;
		
		String val = String.valueOf(xx);
		
		String val11 = String.valueOf(yy);
		
		System.out.println(val+val11);//
	}

}
