package com.test;

public class Exp5 {
	
	public static void main(String[] args) {
		
		//auto boxing
		
		float f = 448.33f;
		
		Float ff = new Float(f);
		
		Float f1 = new Float(40.89);
		
		System.out.println(ff+f1);
		
		System.out.println(f);
		System.out.println(ff);	
		
		// auto unboxing
		
		Double d1 = new Double(50.446);
		
		double d = d1;
		
		System.out.println(d1);
		System.out.println(d);
		
		//Java is a pass by value or reference?
	}

}
