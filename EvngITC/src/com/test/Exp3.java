package com.test;

class G
{
	public G()
	{
		System.out.println("Default");
	}
	public void movie()
	{
		System.out.println("1st movie");
	}
	
	
}

class H extends G
{
	public H()
	{
		super();
		
		System.out.println("Child");
	}
	@Override
	public void movie()
	{
		System.out.println("new movie");
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		G obj = new H();
		
		obj.movie();
		
		
	}

}
