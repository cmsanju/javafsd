package com.test;


class Customer 
{
	int x;
	
	Customer customer;
	
	public Customer()
	{
		this(20);
		System.out.println("Default ");//6
	}
	
	public Customer(int x)
	{
		this(37,"Java");
		this.x = x;
		System.out.println("Parameterised");//5
	}
	
	public Customer(int x, String str)
	{
		System.out.println("overloaded constructor");//4
	}
	
	public void disp(int x)
	{
		//this(30); wrong statement 
		this.x = x;
	}
	
	//static block
	static
	{
		System.out.println("static");//2
	}
	//instane block
	{
		System.out.println("instance");//3
	}
	
	//factory method
	
	public Customer getObject()
	{
		if(customer == null)
		{
			customer = new Customer();
		}
		
		return customer;
	}
	
	@Override
	protected void finalize()
	{
		System.out.println("object destroyed");
	}
}

public class CustomerDemo {
	
	public static void main(String[] args) {
		
		System.out.println("Hi hello");//1
		
		Customer obj = new Customer();
		
		Customer obj2 = new Customer();
		
		Customer obj3 = obj.getObject();
		
		System.out.println(obj.hashCode());
		System.out.println(obj3.hashCode());
		
		obj3 = null;
		
		obj2 = null;
		
		System.gc();
		
		System.out.println("object deleted");
		
		
	}

}
