package com.test;

import java.util.Scanner;

public class Exp1 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter your id");
		
		int id = sc.nextInt();
		
		System.out.println("enter your name");
		
		String name = sc.next();
		
		System.out.println("enter your city");
		
		String city = sc.next();
		
		Student obj = new Student();
		
		obj.setId(id);
		obj.setName(name);
		obj.setCity(city);
		
		System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" City : "+obj.getCity());
	}

}
