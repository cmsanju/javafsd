package com.test;


class Book
{
	public static Book book;
	
	public Book()
	{
		System.out.println("Object created");
	}
	
	public static Book getInstance()
	{
		if(book == null)
		{
			book = new Book();
		}
		
		return book;
	}
}

public class Exp7 {
	
	public static void main(String[] args) {
		
		Book obj1 = Book.getInstance();
		
		Book obj2 = Book.getInstance();
		
		Book obj3 = Book.getInstance();
		
		
		
	}

}
