package com.fls;

import java.io.File;
import java.io.FileOutputStream;

public class FileWrite {
	
	public static void main(String[] args) throws Exception
	{
		File file = new File("src/sample.txt");
		
		FileOutputStream fos = new FileOutputStream(file);
		
		String msg = "Hi this is byte stream file write operation";
		
		fos.write(msg.getBytes());
		
		System.out.println("Done.");
		
	}

}
