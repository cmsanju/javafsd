package com.fls;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class WriteTest {
	
	public static void main(String[] args) throws Exception
	{
		
		FileWriter fw = new FileWriter("src/write.txt");
		
		BufferedWriter bw = new BufferedWriter(fw);
		
		String msg = "This is char stream file write operations";
		
		bw.write(msg);
		
		bw.flush();
		
		System.out.println("Done");
	}

}
