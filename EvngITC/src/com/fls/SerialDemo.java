package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream osb = new ObjectOutputStream(fos);
		
		Employee emp = new Employee();
		
		emp.id = 222;
		emp.name = "Dath";
		emp.city = "Blr";
		emp.pin = 123123;
		
		osb.writeObject(emp);
		
		System.out.println("Done");
	}

}
