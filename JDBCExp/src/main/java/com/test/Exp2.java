package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class Exp2 {
	
	public static void main(String[] args) throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/itcjava", "root", "password");
		
		/*
		PreparedStatement pst = con.prepareStatement("insert into itcstd values(?,?,?)");
		
		pst.setInt(1, 3);
		pst.setString(2, "Sachin");
		pst.setString(3, "Bangalore");
		
		pst.execute();
		
		
		
		PreparedStatement pst = con.prepareStatement("update itcstd set std_name=? where id =?");
		
		pst.setString(1, "Sounak Sarkar");
		
		pst.setInt(2, 3);
		
		pst.execute();
		
		
		
		PreparedStatement pst = con.prepareStatement("delete from itcstd where id = ?");
		
		pst.setInt(1, 3);
		
		pst.execute();
		
		*/
		
		PreparedStatement pst = con.prepareStatement("select * from itcstd");
		
		ResultSet rs = pst.executeQuery();
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		
		System.out.println("Done.");
		
		con.close();
	}

}
