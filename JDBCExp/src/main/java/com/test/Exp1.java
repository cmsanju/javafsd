package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//step1 : load the driver class
		
		Class.forName("com.mysql.jdbc.Driver");
		
		//Step2 : create connection object
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/itcjava", "root", "password");
		
		//step3 : create statement object
		
		Statement stmt = con.createStatement();
		
		//step4 execute the query
		
		//stmt.execute("create table itcstd(id int, std_name varchar(25), city varchar(25))");
		
		String sql2 = "insert into itcstd values(3, 'Sachin', 'Bangalore')";
		
		String sql3 = "update itcstd set std_name = 'Hero' where id = 3";
		
		String sql1 = "delete from itcstd where id=3";
		
		String sql4 = "insert into itcstd values(4, 'Sarkar', 'Kolkata')";
		
		stmt.addBatch(sql2);
		stmt.addBatch(sql3);
		stmt.addBatch(sql1);
		stmt.addBatch(sql4);
		
		stmt.executeBatch();
		
		String sql = "select * from itcstd";
		
		ResultSet rs =  stmt.executeQuery(sql);
		
		ResultSetMetaData rsd = rs.getMetaData();
		
		System.out.println("Total columns : "+rsd.getColumnCount());
		System.out.println("Column name first one : "+rsd.getColumnName(1));
		System.out.println("Column type 1st one : "+rsd.getColumnTypeName(1));
		
		while(rs.next()) {
		
		System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		
		}
		
		//step 5 close the connection object
		
		con.close();
	}

}
