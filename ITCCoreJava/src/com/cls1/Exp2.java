package com.cls1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Student
{
	int id;
	String name;
	String city;
	
	public Student(int id, String name, String city)
	{
		this.id = id;
		this.name = name;
		this.city = city;
	}
	/*
	public String toString()
	{
		return this.id+" "+this.name+" "+this.city;
	}
	*/

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", city=" + city + "]";
	}
}

class SortById implements Comparator<Student>
{
	public int compare(Student o1, Student o2)
	{
		return o1.id - o2.id;
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		ArrayList<Student> stdList = new ArrayList<Student>();
		
		stdList.add(new Student(101, "jAVA", "oRACLE"));
		stdList.add(new Student(10, "hELLO", "BLR"));
		stdList.add(new Student(34,"sPRING", "hyd"));
		stdList.add(new Student(5, "user", "Blr"));
		
		System.out.println(stdList);
		
		//Collections.sort(SortById.class);
	}

}
