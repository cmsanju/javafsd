package com.cls1;

import java.util.ArrayList;
import java.util.Collections;

public class Exp1 {
	
	public static void main(String[] args) {
		
		ArrayList<String> list = new ArrayList<String>();
		
		list.add("java");
		list.add("dot net");
		list.add("python");
		list.add("spring");
		list.add("c++");
		list.add("hibernate");
		
		System.out.println(list);
		
		//sorted order
		
		Collections.sort(list);
		System.out.println(list);
	}

}
