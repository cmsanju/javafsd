package com.fls;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class WriteFile {
	
	public static void main(String[] args) throws Exception
	{
		//File file = new File("src/sample.txt");
		
		FileWriter fw = new FileWriter("src/sample.txt");
		
		BufferedWriter bw = new BufferedWriter(fw);
		
		String msg = "Hi this is File writer example ";
		
		bw.write(msg);
		
		bw.flush();
		
		System.out.println("Success.");
	}

}
