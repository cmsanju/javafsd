package com.fls;

import java.io.File;
import java.io.FileOutputStream;

public class WriteTest 
{
	public static void main(String[] args) 
	{
		try
		{
		
			File file = new File("src/write.txt");
		
			FileOutputStream fos = new FileOutputStream(file);
			
			String msg = "hi this is byte stream file write operation";
			
			fos.write(msg.getBytes());
			
			System.out.println("Success.");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
	}
}
