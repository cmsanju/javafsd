package com.fls;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

public class ReadTest {
	
	public static void main(String[] args) throws Exception
	{
		File file = new File("src/write.txt");
		
		FileInputStream fis = new FileInputStream(file);
		
		BufferedInputStream bis = new BufferedInputStream(fis);//needs to read the data from buffer
		
		byte[] br = new byte[1024];//buffer
		
		int x = 0;
		
		while((x = bis.read(br)) != -1)//true reached to 0th index
		{
			System.out.println(new String(br,0,x));
		}
		
	}

}
