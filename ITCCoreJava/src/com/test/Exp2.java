package com.test;

interface Inf1
{
	 void pet();
	 
	 void animal();
	
}

abstract class Abs
{
	public void show()
	{
		System.out.println("abs implemented method");
	}
	
	public abstract void movie();
}

class Impl extends Abs implements Inf1
{
	public void movie()
	{
		System.out.println("abs overrided");
	}
	
	public void pet()
	{
		System.out.println("inf overrided");
		
	}
	
	public void animal()
	{
		System.out.println("inf overrided");
	}
	
	public void cat()
	{
		System.out.println("CAT FROM NORMAL");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		//Inf1 obj = new Inf1();
		
		//Abs obj = new Abs();
		
		Impl obj = new Impl();
		
		obj.show();
		obj.pet();
		obj.movie();
		obj.animal();
		
		obj.cat();
		
		
	}

}
