package com.test;


class Employee
{
	public Employee()
	{
		System.out.println("Default constructor");
	}
	
	public Employee(int x)
	{
		System.out.println("Parameterised constructor");
	}
	public Employee(double y)
	{
		System.out.println("Overloaded constructor");
	}
	
	public Employee(Employee emp)
	{
		System.out.println("object parameterised constructor");
	}
}


public class Exp3 {
	
	public static void main(String[] args) {
		
		Employee emp1 = new Employee();
		
		Employee emp2 = new Employee(20);
		
		Employee emp3 = new Employee(45.67);
		
		Employee emp4 = new Employee(emp1);
		
	}

}
