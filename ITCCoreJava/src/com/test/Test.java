package com.test;

class TestDebug
{
	public void add()
	{
		System.out.println("Test");
	}
	
	public void show()
	{
		System.out.println("HELLO");
	}
	
}

public class Test {
	
	public static void main(String[] args) {
		
		TestDebug objDebug = new TestDebug();
		
		objDebug.add();
		objDebug.show();
		
	}

}
