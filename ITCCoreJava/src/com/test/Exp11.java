package com.test;

class A
{
	static 
	{
		System.out.println("1");
	}
	
	{
		System.out.println("2");
	}
	
	A()
	{
		this(100);
		System.out.println("3");
	}
	A(int a){
	//	this();
		System.out.println("4");
	}
}

public class Exp11 {
	
	Exp11 e = new Exp11();
	
	public static void main(String[] args) {
		//System.out.println("5");
		
		//new A(200);
		
		Exp11 e = new Exp11();
	}

}
