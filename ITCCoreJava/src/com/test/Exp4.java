package com.test;

class Customer
{
	public Customer()
	{
		this("ITC");
		System.out.println("Default constructor");//4
	}
	
	public Customer(String name)
	{
		this(20);
		System.out.println("String arg construtor");//3
	}
	
	public Customer(int y)
	{
		this(34,"JAVA");
		System.out.println("int arg constructor");//2
	}
	
	public Customer(int x, String str)
	{
		System.out.println("Double arg constructor");//1
	}
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		Customer cst = new Customer();
		
	}

}
