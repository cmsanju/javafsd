package com.test;

public class Exp9 {
	
	public static void main(String[] args) {
		
		String str = "ITC";
		
		for(int i = str.length()-1; i >= 0; i--)
		{
			System.out.print(str.charAt(i));
		}
		
		System.out.println();
		
		StringBuffer sb = new StringBuffer(str);
		
		System.out.println(sb.reverse());
		
		sb.append(" 1.8");
		
		System.out.println(sb);
	}

}
/*
 * 1 Student Example Using Scanner class for user friendly inputs(id, name, marks, location, finally )
 * 2 Write a simple programme to accept inputs from user and validate for calculating marks and display 
 * the records upper case one output / lower and upper case
 * 
 */
