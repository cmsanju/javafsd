package com.test;

class SingleTon
{
	private static SingleTon singlObj = null;
	
	public String cmpName;
	
	private SingleTon()
	{
		cmpName = "ITC Infotech";
	}
	
	public static SingleTon getInstance()
	{
		if(singlObj == null)
		{
			synchronized (singlObj) {
				
				singlObj = new SingleTon();
			}
			
		}
		
		return singlObj;
	}
}

public class Exp8 {
	
	public static void main(String[] args) {
		
		String str = "java";
		String str2 = "java";
		
		String str1 = new String("java");
		
		System.out.println(str == str2);//true or false
		
		System.out.println(str.equals(str1));
		
	}

}
