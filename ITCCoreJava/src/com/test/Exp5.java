package com.test;

class Student
{
	int x = 500;//instance/non static data is object level data
	
	static int y = 570;//static data is class level data
	
	//static data is nothing but one time memory allocation 
	public Student()
	{
		System.out.println("Default constructor");
	}
	
	//instance block
	{
		System.out.println("instance block1");
	}
	
	{
		System.out.println(Student.y);
		
		System.out.println("instance block2");
	}
	
	static
	{
		Student std = new Student();
		
		System.out.println(std.x);
		
		System.out.println("static block1");
	}
	
	static
	{
		System.out.println("sttic block2");
	}
	
	public Student getInstance()
	{
		
		return new Student();
	}
	
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		Student std = new Student();
		
	}

}
