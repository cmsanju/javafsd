package com.cls;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Exp2 {
	
	public static void main(String[] args) {
		
		List data = new LinkedList();
		
		data.add(10);
		data.add("java");
		data.add('A');
		data.add("java");
		data.add(45.34f);
		data.add(74.74);
		data.add(false);
		data.add(10);
		
		System.out.println(data);
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
