package com.cls;

import java.util.TreeSet;

public class Exp5 {
	
	public static void main(String[] args) {
		
		TreeSet<Integer> data = new TreeSet<>();
		
		data.add(10);
		data.add(23);
		data.add(5);
		data.add(11);
		data.add(2);
		data.add(45);
		data.add(9);
		data.add(1);
		
		System.out.println(data);
		
		TreeSet<String> data1 = new TreeSet<String>();
		
		data1.add("hello");
		data1.add("asus");
		data1.add("mango");
		data1.add("apple");
		data1.add("sony");
		data1.add("macOS");
		data1.add("lenovo");
		data1.add("zero");
		
		System.out.println(data1);
		
		
		
	}

}
