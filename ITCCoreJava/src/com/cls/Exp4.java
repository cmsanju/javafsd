package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class Exp4 {
	
	public static void main(String[] args) {
		
		HashSet data = new HashSet();
		
		data.add(10);//HashMap -> put(add(), "preset"); -> array of buckets (10) -> override hashCode and equals 
		data.add("java");
		data.add('A');
		data.add("java");
		data.add(45.34f);
		data.add(74.74);
		data.add(false);
		data.add(10);
		
		System.out.println(data);
		
		LinkedHashSet data1 = new LinkedHashSet();
		
		data1.add(10);
		data1.add("java");
		data1.add('A');
		data1.add("java");
		data1.add(45.34f);
		data1.add(74.74);
		data1.add(false);
		data1.add(10);
		
		System.out.println(data1);
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
