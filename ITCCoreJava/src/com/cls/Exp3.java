package com.cls;

import java.util.Stack;

public class Exp3 {
	
	
	public static void main(String[] args) {
		
		Stack data = new Stack();//LIFO
		
		
		data.add(10);
		data.add("java");
		data.add('A');
		data.add("java");
		data.add(45.34f);
		data.add(74.74);
		data.add(false);
		data.add(10);
		data.add("hello");
		
		System.out.println(data);
		
		System.out.println(data.peek());//hello
		
		System.out.println(data.pop());
		
		System.out.println(data);
		
		data.push("Biswajit");
		
		System.out.println(data.peek());
		
		System.out.println(data.search(100));
		
		data.clear();
		
		System.out.println(data.empty());
		
	}

}
