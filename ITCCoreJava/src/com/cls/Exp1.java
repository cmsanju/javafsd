package com.cls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//Collection -> List
		
		//Collection data = new ArrayList();
		
		List data = new ArrayList();
		
		data.add(10);
		data.add("java");
		data.add('A');
		data.add("java");
		data.add(45.34f);
		data.add(74.74);
		data.add(false);
		data.add(10);
		
		System.out.println(data);
		
		data.add(20);
		
		System.out.println(data);
		
		System.out.println(data.size());
		
		//System.out.println(data.remove(10));
		
		System.out.println(data.size());
		
		System.out.println(data.contains('B'));
		
		//Iterator / ListIterator
		//hasNext(), next(), remove() / hasPrevious(), previous()
		
		//Iterator itr = data.iterator();
		
		ListIterator itr = data.listIterator();
		
		System.out.println("Forward");
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		System.out.println("reverse");
		
		while(itr.hasPrevious())
		{
			System.out.println(itr.previous());
		}
	}

}
