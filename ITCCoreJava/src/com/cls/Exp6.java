package com.cls;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class Exp6 {
	
	public static void main(String[] args) {
		
		//HashMap<String, Integer> data = new HashMap<>();//1.2v 
		
		Hashtable<String, Integer> data = new Hashtable<String, Integer>();//1.0v
		
		data.put("Sony", 2000);
		data.put("Lenovo", 450);
		data.put("asus", 478);
		data.put("macOS", 5000);
		data.put("dell", 378);
		data.put("thinkpad", 3456);
		data.put("iball", 890);
		
		System.out.println(data);
		
		LinkedHashMap<String, Integer> data1 = new LinkedHashMap<>();
		
		
		data1.put("Sony", 2000);
		data1.put("Lenovo", 450);
		data1.put("asus", 478);
		data1.put("macOS", 5000);
		data1.put("dell", 378);
		data1.put("thinkpad", 3456);
		data1.put("iball", 890);
		data1.put("dell", 5000);
		
		System.out.println(data1);
		
		Iterator<Entry<String, Integer>> itr = data1.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Product : "+et.getKey()+" Price : "+et.getValue());
		}
		
		for(String key : data.keySet())
		{
			System.out.println(key+" "+data1.get(key));
		}
	}

}
