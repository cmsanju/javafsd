package com.inh;

class A
{
	int id  = 111;
	String name = "Java";
	
	public void show()
	{
		System.out.println(id+" "+name);
		
		System.out.println("Parent method");
	}
}

class B extends A
{
	String cmp = "ITC";
	
	public void disp()
	{
		System.out.println(id+" "+name+" "+cmp);
	}
}

public class Exp1 
{
	public static void main(String[] args) {
		
		B b = new B();
		
		b.show();
		b.disp();
	}
}
