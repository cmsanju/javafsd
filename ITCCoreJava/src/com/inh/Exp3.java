package com.inh;

class E
{
	public void human()
	{
		System.out.println("class E");
	}
}

class F extends E
{
	public void animal()
	{
		System.out.println("class F");
	}
}

class G extends E
{
	public void pets()
	{
		System.out.println("class G");
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		F f = new F();
		
		f.animal();
		f.human();
		
		G g = new G();
		
		g.pets();
		g.human();
		
		E e1 = (E)g;// up casting
		
		E e = (E)new G();//
		
		e.human();
		
		G g1 = (G)new E();// down casting 
		
		int x = 1000;
		
		long l = x;
		
		//byte -> short -> int -> long --> 
		//byte <- short <- int <- long --> 
		
	}

}
