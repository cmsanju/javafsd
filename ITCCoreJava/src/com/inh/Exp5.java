package com.inh;

class Poly
{
	public void draw()
	{
		System.out.println("Tle");
	}
}

class Child1 extends Poly
{
	@Override
	public void draw()
	{
		System.out.println("Rle");
	}
}

class Child2 extends Poly
{
	@Override
	public void draw()
	{
		System.out.println("Cle");
	}
}



public class Exp5 {
	
	public static void main(String[] args) {
		
		Child1 c1 = new Child1();//TWO TIMES MEMORY 
		
		c1.draw();//Rle
		
		Child2 c2 = new Child2();
		
		c2.draw();//Cle
		
		Poly p1 = new Child1();//Dynamic binding 
		
		p1.draw();//Rle
		
		Poly p2 = new Child2();
		
		p2.draw();//Cle
	}
}
