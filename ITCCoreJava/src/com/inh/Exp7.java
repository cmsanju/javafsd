package com.inh;

interface Inf6
{
	void add();
	
	interface Inf7
	{
		void sub();
	}
}

class Impl1 implements Inf6.Inf7
{
	public void sub() {
		
		System.out.println("inner inf");
	}
	
	
}

public class Exp7 {
	
	public static void main(String[] args) {
		
	}

}
