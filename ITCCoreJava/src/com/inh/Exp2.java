package com.inh;


class C
{
	public void cat()
	{
		System.out.println("parent C");
	}
}

class D extends C
{
	public void dog()
	{
		System.out.println("Inmt parent D");
	}
}

class N extends D
{
	public void pet()
	{
		System.out.println("Bottom most child class");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		N n = new N();
		
		n.cat();
		n.dog();
		n.pet();
	}

}
