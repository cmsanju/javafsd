package com.inh;

interface Inf5
{
	float pi = 3.14f;//public static final variables
	
	void movie();
}

interface Infn
{
	
}

abstract class Hello
{
	public void disp()
	{
		System.out.println("ABS normal method");
	}
	
	public abstract void show();
	
}

class Impl extends Hello implements Inf5,Infn
{
	@Override
	public void show()
	{
		System.out.println("abs overrided");
	}
	
	@Override
	public void movie()
	{
		System.out.println("inf overrided");
	}
}

public class Exp6 {
	
	public static void main(String[] args) {
		
		Impl i = new Impl();
		
		i.disp();
		i.show();
		i.movie();
	}

}
