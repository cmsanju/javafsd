package com.inh;

@FunctionalInterface
interface FunTest
{
	String cat(int x);
	
	default void disp()
	{
		
	}
	static void show()
	{
		
	}
}

public class Exp8 {
	
	public static void main(String[] args) {
		
		FunTest obj = new FunTest() {
			
			public String cat(int x)
			{
				System.out.println("overrided");
				
				return "java";
			}
		};
		obj.cat(20);
		
		FunTest obj1 = (int x) -> {
			System.out.println("lambda expressions");
			
			return "java";
			
		};
		obj1.cat(30);
		}	
	}


