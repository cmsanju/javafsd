package com.excp;

public class Exp3 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(200/0);
		}
		catch(Exception e)
		{
			//using getMessage();
			
			System.out.println(e.getMessage());// only exception message
			
			//printing exception class object
			
			System.out.println(e);//class name and message
			
			//using printStackTrace()
			
			e.printStackTrace();
			
		}
	}

}
