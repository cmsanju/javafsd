package com.excp;

public class Exp1 {
	
	public static void main(String[] args) {
		
		try {
			
		System.out.println(120/5);
		
		System.out.println("Test");
		
		String name = "java";
		
		System.out.println(name.charAt(2));
		
		System.out.println("Hello");
		
		String pwd = null;
		
		System.out.println(pwd.equals("java"));
		
		}
		catch(ArithmeticException ae)
		{
			System.out.println("don't enter zero for den");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check your name length");
		}
		catch(NullPointerException npe)
		{
			System.out.println("please enter password");
		}
		catch (Exception e) {
			
			System.out.println("check your inputs");
		}
		finally
		{
			System.out.println("i am from finally");
		}
		
		
	}

}
