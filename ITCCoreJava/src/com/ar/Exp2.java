package com.ar;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Pattern p1 = Pattern.compile(".j");
		
		Matcher m1 = p1.matcher(".j");
		
		boolean b1 = m1.matches();
		
		System.out.println(b1);
		
		
		boolean b2 = Pattern.compile(".j").matcher(".j").matches();
		
		System.out.println(b2);
		
		System.out.println(Pattern.matches(".J", "aju"));//
		
		
		System.out.println(Pattern.matches("[javk]*", "kk"));
		
		System.out.println(Pattern.matches("\\d", "1"));
		
		System.out.println(Pattern.matches("\\D", "a"));
		
		System.out.println(Pattern.matches("[a-zA-Z0-9]{8}", "Java1234"));
		System.out.println(Pattern.matches("[0-9][a-z][A-Z][*^&$#@()]{8}", ""));
	}

}
