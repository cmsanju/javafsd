package com.ar;

public class Exp1 {
	
	public static void main(String[] args) {
		
		int[] ar = {12,34,56,78,98};
		
		System.out.println(ar[0]);//12
		System.out.println(ar[1]);
		System.out.println(ar[2]);
		System.out.println(ar[3]);
		System.out.println(ar[4]);
		
		//enhanced for loop
		for(int x : ar)
		{
			System.out.println(x);
		}
		
		String[] names = {"java", "php", "Spring"};
		
		for(String list : names)
		{
			System.out.println(list);
		}
		
		int[] ar1 = new int[]{12,12,34,34};
		
		int[] ar2 = new int[5];// 5 (0-4)
		
		 ar2[4] = 40;
		 
		 System.out.println(ar2[4]);
		 
		 int[][] dbar = new int[10][20];
		 
		// int[][][] tdar = new int[5][10][20];
		 
		 int[][] twar = {{1,2,3}, {3,4,5}, {5,6,7}};
		 
		 System.out.println(twar[0][0]);
		 System.out.println(twar[1][1]);
		 System.out.println(twar[0][2]);
		 System.out.println(twar[2][2]);
		 
		 for(int i = 0; i<3; i++)
		 {
			 for(int j = 0; j<3; j++)
				 System.out.print(twar[i][j]+" ");
			
			 System.out.println();
		 }
	}

}
