package com.ar;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exp3 {
	
	public static boolean passwordValidate(String givenInput)
	{
		String expectedInput = "^(?=.*[0-9])"+"(?=.*[a-z])(?=.*[A-Z])"+"(?=.*[@#$%&])"+"(?=.\\S+$).{8,20}$";
		
		Pattern p1 = Pattern.compile(expectedInput);
		
		if(givenInput == null)
		{
			return false;
		}
		
		Matcher m1 = p1.matcher(givenInput);
		
		return m1.matches();		
	}
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter password");
		
		String pass = sc.next();
		
		boolean b1 = passwordValidate(pass);
		
		System.out.println(b1);
	}
}
