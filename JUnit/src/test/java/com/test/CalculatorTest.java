package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculatorTest {
	
	Calculator calculator;
	
	@BeforeClass
	public static void beforeClass()
	{
		System.out.println("Before Class");
	}
	
	@AfterClass
	public static void afterClass()
	{
		System.out.println("After Class");
	}
	
	@Before
	public void setUp()
	{
		calculator = new Calculator();
		
		System.out.println("Before test");
	}
	
	@After
	public void setDown()
	{
		calculator = null;
		
		System.out.println("After Test");
	}
	
	@Test
	public void testAdd()
	{
		assertEquals(50, calculator.add(20, 30));
		
		System.out.println("TEST METHOD ADD");
	}
	@Test
	public void testSub()
	{
		assertEquals(30, calculator.sub(50, 20));
		
		System.out.println("TEST METHOD SUB");
	}
	
	@Test
	public void testNamesCheck()
	{
		assertEquals("java", calculator.namesCheck("java"));
		
		System.out.println("NAMES METHOD");
	}

}
