package com.test;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


@WebListener
public class MyListener implements HttpSessionListener {

	public static int current, total;
	
	ServletContext ctx = null;
   
    public void sessionCreated(HttpSessionEvent se)  { 
    	
    	current++;
    	total++;
    	
    	ctx = se.getSession().getServletContext();
    	
    	ctx.setAttribute("currentusers", current);
    	ctx.setAttribute("totalusers", total);
         
    }

    public void sessionDestroyed(HttpSessionEvent se)  { 
    	
    	current --;
    	
    	ctx.setAttribute("currentusers", current);
         
    }
	
}
