package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Servlet1")
public class Servlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String user = request.getParameter("user");
		String pwd = request.getParameter("pwd");
		
		HttpSession session = request.getSession();
		
		session.setAttribute("info", user);
		
		ServletContext ctx = getServletContext();
		
		int total = (Integer)ctx.getAttribute("totalusers");
		
		int current = (Integer)ctx.getAttribute("currentusers");
		
		out.println("Total users : "+total);
		
		out.println("<br>Current users : "+current);
		
		out.println("<br><a href = 'Logout'> log out </a>");
		
	}

}
