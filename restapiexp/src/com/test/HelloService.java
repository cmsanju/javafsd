package com.test;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/hello")
public class HelloService 
{
	@GET
	@Path("/text")
	@Produces(MediaType.TEXT_PLAIN)
	public String helloResponse()
	{
		return "Hi this is plaint text REST API response";
	}
	
	@GET
	@Path("/html/{name}")
	@Produces(MediaType.TEXT_HTML)
	public String helloHtmlResponse(@PathParam("name") String name)
	{
		return "<html> <body> <h1> This is thml response  "+name+" </h1> </body> </html>";
	}
	
	@POST
	@Path("/createEmp")
	public Response empDetails(@FormParam("id") int id, @FormParam("name")
	String name, @FormParam("cmp") String cmp)
	{
		return Response.status(200).entity("Employee Details <br> ID "
				+ ""+id+"<br> Name "+name+" <br> Company "+cmp).build();
	}
	
	
}

/**
 * @pathParam
 * @GET  (SELECT QUERY)
 * @POST (INSERT) CREATING THE RESOURCE
 * @PUT (UPDATE QUERY) UPDATE THE RECORDS
 * @DELETE (DELETE QUERY) DELETING THE RECORD FROM THE RESOURCE
 * 
 * 
 */
