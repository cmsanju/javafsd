package jspcustomtag;

import java.util.Date;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class ITCDate extends TagSupport
{
	public int doStartTag() {
		
		JspWriter out = null;
		
		try
		{
			out = pageContext.getOut();
			
			Date date = new Date();
			
			out.println("Current DATE : "+date);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return SKIP_BODY;
	}
}
