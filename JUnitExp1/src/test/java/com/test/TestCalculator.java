package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestCalculator {
	
	Calculator obj;
	
	@BeforeClass
	public static void beforeClass()
	{
		System.out.println("before class");
	}
	
	@AfterClass
	public static void afterClass()
	{
		System.out.println("after class");
	}
	
	@Before
	public void setUp()
	{
		System.out.println("before test method");
		
		obj = new Calculator();
	}
	
	@After
	public void setDown()
	{
		System.out.println("after test method");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("test method add");
		
		assertEquals(90, obj.add(50, 40));
	}
	@Test
	public void testSub()
	{
		System.out.println("test method sub");
		
		assertEquals(100, obj.sub(200, 100));
	}
	
	@Test
	public void testDiv()
	{
		System.out.println("test method div");
		
		assertEquals(50, obj.div(100, 2));
	}

}
